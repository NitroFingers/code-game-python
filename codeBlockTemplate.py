class template(codeBlock):
	def __init__(self, code, real=True):
		self.code = code
		self.real = real
		codeBlock.__init__(self)
		self.caption = self.code.nameFont.render('Template Block', True, (0,0,0))
		self.color = (0,0,255)

	def settingsMenu(self):
		menu = Menu('edit')
		dim = self.code.world.main.dimensions
		rect = (dim[0] / 2 - 200,dim[1] / 2 - 150,400,300)
		menu.setRect(rect)
		menu.setCaption('Edit Menu')
		#menu.addSetting(name='', Type='selection',value=self.toMove,selectionList=['Up','Down'], target=self)
		self.code.world.main.menuManager.addMenu(menu)

	def setValue(self, value):
		pass

	def draw(self, rect=None):
		if rect == None:
			rect = self.rect

		surface = self.code.world.main.screen
		rect = (rect[0],rect[1], self.caption.get_width(),rect[3])
		pygame.draw.rect(surface,self.color, rect,0)
		pos = (rect[0],rect[1])
		surface.blit(self.caption,(pos[0],pos[1] - 1))

		self.rect = rect
		return 30

	def exec(self):
		print('Template')