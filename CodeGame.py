from pygame.locals import *
import pygame
from world import *
from menu import MenuManager
import json
from os import listdir
from os.path import isfile, join



main_menu = 'main_menu'
game_state = 'gameState'

class Main:
	def __init__(self):
		pygame.init()
		self.dimensions = (1280,800)

		#self.screen = pygame.display.set_mode(self.dimensions)
		self.screen = pygame.display.set_mode(self.dimensions)

		pygame.display.set_caption('Code Game')
		self.done = False

		self.menuManager = MenuManager(self)

		self.state = main_menu

		self.loadStartup()

		self.dirtyRects = []

	def loadStartup(self):
		menu = Menu('menu')
		dim = self.dimensions
		rect = (dim[0] / 2 - 200,dim[1] / 2 - 150,400,300)
		menu.setRect(rect)
		menu.setCaption('Main menu')
		menu.cannotClose()
		menu.addItem(name='Level Select', Type='button', target=self.loadLevelSelect)
		self.menuManager.addMenu(menu)

	def loadLevelSelect(self):
		self.menuManager.destroyAll()
		menu = Menu('menu')
		dim = self.dimensions
		rect = (dim[0] / 2 - 200,dim[1] / 2 - 150,400,300)
		menu.setRect(rect)
		menu.setCaption('Level Select')

		onlyfiles = [f for f in listdir('levels') if isfile(join('levels', f))]
		print(onlyfiles)

		for levelFile in onlyfiles:
			path = os.path.join('levels', levelFile)
			with open(path) as data_file:
				levelData = json.load(data_file)
				menu.addItem(name=levelData['name'], Type='button', target=self.loadWorld, args=levelFile)

		menu.cannotClose()
		self.menuManager.addMenu(menu)

	def loadWorld(self, level):
		self.menuManager.destroyAll()

		self.world = World(self, level)
		self.state = game_state

	def checkEvents(self):
		for event in pygame.event.get():
			if event.type == QUIT:
				self.breakThreads()
				self.quit()
				return True

			elif event.type == MOUSEBUTTONDOWN:
				if event.button == 1:
					self.doMouseDownEvent()

			elif event.type == MOUSEBUTTONUP:
				if event.button == 1:
					self.doMouseUpEvent()
				elif event.button == 3:#RIGHT CLICK
					if self.state == game_state:
						self.world.rightClick(pygame.mouse.get_pos())
				elif event.button == 4:
					if self.state == game_state:
						self.world.code.scrollUp()
				elif event.button == 5:
					if self.state == game_state:
						self.world.code.scrollDown()
		return False

	def breakThreads(self):
		self.world.code.breakThread = True
		print('Returning all threads...')

	def doMouseDownEvent(self):
		pos = pygame.mouse.get_pos()
		if not self.menuManager.mouseDown(pos):
			if self.state == game_state:
				self.world.mouseDown(pos)

	def doMouseUpEvent(self):
		self.menuManager.mouseUp()
		if self.state == game_state:
			self.world.mouseUp(pygame.mouse.get_pos())


	def loop(self):
		self.screen.fill((255,255,255))
		mousePos = pygame.mouse.get_pos()

		if self.state == game_state:
			self.world.draw()
			self.world.update()
			self.world.mouseUpdate(mousePos)

		self.menuManager.update(mousePos)
		self.menuManager.draw()

		pygame.display.flip()
		#pygame.display.update(self.dirtyRects)
		self.dirtyRects = []

		return self.checkEvents()

	def quit(self):
		pygame.quit()
		self.done = True

if __name__ == '__main__':

	print('Initializing...')
	main = Main()
	while not main.done:
		done = main.loop()
	main.quit()
	print('Loop exit')
else:
	pass
