import pygame

class Cube():
	def __init__(self, color=(0,255,0)):
		self.color = color
		self.size = 80


	def draw(self, surface, pos):
		rect = (pos[0] - (self.size / 2),pos[1] - (self.size / 2),self.size,self.size)
		pygame.draw.rect(surface, self.color, rect,0)
		return self.size

class Destroyer():
	def __init__(self, color=(25,25,0)):
		self.color = color
		self.size = 60


	def draw(self, surface, pos):
		rect = (pos[0] - (self.size / 2),pos[1] - (self.size / 2),self.size,self.size)
		pygame.draw.rect(surface, self.color, rect,0)
		return self.size

class cubeStack():
	def __init__(self, pos=(100,100)):
		self.cubeList = []
		self.pos = pos


	def draw(self, surface):
		xOffset = 0
		for cube in self.cubeList:
			pos = (self.pos[0] + xOffset, self.pos[1])
			xOffset -= cube.draw(surface, pos) + 5

	def grab(self):
		if len(self.cubeList) != 0:
			cube = self.cubeList[-1]
			self.cubeList.pop(-1)
			return cube
		return None

	def drop(self,cube):
		self.cubeList.append(cube)

	def removeLast(self):
		if len(self.cubeList) > 0:
			self.cubeList.pop(len(self.cubeList) - 1)
			return True
		else:
			return False


class Char():
	def __init__(self, world):
		self.world = world

		dim = self.world.main.dimensions
		self.pos = (40, int(dim[1] / 2))

		self.stackPos = 0
		self.carryCube = None
		self.cubeStacks = []

	def reset(self):
		self.stackPos = 0
		self.carryCube = None

	def spawnStacks(self,stackCount=4):
		dim = self.world.main.dimensions
		self.cubeStacks = []
		for i in range(0,stackCount):
			pos = (int(dim[0] / 2 * 1) - 45, int((dim[1]/stackCount * i) + (dim[1] / stackCount / 2)))
			self.cubeStacks.append(cubeStack(pos=pos))

		self.stackCount = stackCount

	def draw(self):
		surface = self.world.main.screen

		self.pos = (self.pos[0], self.cubeStacks[self.stackPos].pos[1])
		pygame.draw.circle(surface, (16, 239, 120), self.pos, 40, 0)

		for stack in self.cubeStacks:
			stack.draw(surface)

		if self.carryCube != None:
			pos = (120, self.pos[1])
			self.carryCube.draw(surface, pos)

	def performAction(self,action):
		if (action == 'Grab') and (self.carryCube == None):
			print('Grabbing')
			self.carryCube = self.cubeStacks[self.stackPos].grab()
			print(self.carryCube)

		elif (action == 'Drop') and (self.carryCube != None):
			print('Dropping')
			if (type(self.carryCube) == Destroyer):
				#self.cubeStacks.pop(len(self.cubeStacks) - 1)
				if self.cubeStacks[self.stackPos].removeLast():
					pass
				else:
					self.cubeStacks[self.stackPos].drop(self.carryCube)
				self.carryCube = None
			else:
				self.cubeStacks[self.stackPos].drop(self.carryCube)
				self.carryCube = None



	def move(self, action):
		if action == 'Up':
			self.stackPos -= 1
			if self.stackPos < 0:
				self.stackPos = 0
		elif action == 'Down':
			self.stackPos += 1
			if self.stackPos > self.stackCount - 1:
				self.stackPos = self.stackCount - 1

	def getCarryColor(self):
		if self.carryCube != None:
			return self.carryCube.color
		return False
