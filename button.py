import pygame

def range_overlap(a_min, a_max, b_min, b_max):
			overlapping = True
			if (a_min > b_max) or (a_max < b_min):
				overlapping = False
			return overlapping

class Button:
	def __init__(self,parent, pos):
		self.parent = parent
		
		self.pos = pos
		self.action = None
		self.args = None


	def setCaption(self, caption):
		font = pygame.font.SysFont("verdana", 18)
		self.caption = font.render(caption, True, (0,0,0))

	def draw(self, surface):
		rect = (self.pos[0],self.pos[1],self.caption.get_width(),20)
		pygame.draw.rect(surface, (250,37,40), rect,0)
		surface.blit(self.caption,self.pos)

	def checkPress(self, pos):
		rect = (self.pos[0],self.pos[1],self.caption.get_width(),20)
		if range_overlap(rect[0],rect[2] + rect[0],pos[0],pos[0]) and range_overlap(rect[1],rect[3] + rect[1],pos[1],pos[1]): 
			if self.action is not None:
				if self.args  is None:
					self.action()
				else:
					self.action(self.args)
			else:
				print(self, ' has no action')
			return True
		return False