import sys
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {"packages": ["os"], "excludes": ["tkinter","numpy","curses","multiprocessing","socket" "email", "html", "pydoc_data", "xml","xml_rpc","distutils","unittest","urllib","pkg_resources","logging","ssl","lzma","http"]}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(  name = "CodeGame",
        version = "1.1",
        description = "CodeGameV1.1!",
        options = {"build_exe": build_exe_options},
        executables = [Executable("CodeGame.py", base=base)])
