import pygame
import time
from code import *
from char import *
import json, os
from menu import *
from itertools import zip_longest

def LoadLevel(world, toLoad):
	print('Loading level ', toLoad)
	path = os.path.join('levels', toLoad)
	print(path)
	with open(path) as data_file:
		level = json.load(data_file)

	print(level['name'])

	world.char.cubeStacks = []
	dim = world.main.dimensions
	stackCount = len(level['stacks'])

	i = 0
	for stack in level['stacks']:
		pos = (int(dim[0] / 2 * 1) - 45, int((dim[1]/stackCount * i) + (dim[1] / stackCount / 2)))

		thisStack = cubeStack(pos=pos)
		for cube in stack['cubes']:
			if cube['type'] == 'cube':
				color = (cube['color'][0],cube['color'][1],cube['color'][2])
				cu = Cube(color)
				cu.ID = cube["ID"]
				thisStack.drop(cu)
			else:
				color = (cube['color'][0],cube['color'][1],cube['color'][2])
				cu = Destroyer(color)
				cu.ID = cube["ID"]
				thisStack.drop(cu)

		world.char.cubeStacks.append(thisStack)

		world.char.stackCount = stackCount

		i += 1

def checkCompletion(world, level):
	path = os.path.join('levels', level)
	print(path)
	with open(path) as data_file:
		level = json.load(data_file)

	print('Checking completion...')

	i = 0
	for jStack, worldStack in zip(level['levelComplete'], world.char.cubeStacks):
		print('	Checking stack: ', jStack,worldStack)
		for jCube, worldCube in zip_longest(jStack['cubes'], worldStack.cubeList):
			if (worldCube != None) and (jCube != None):
				if jCube["ID"] == worldCube.ID:
					print('Cube ', worldCube.ID, ' is present')
				else:
					print('Incorrect ID')
					return False
			else:
				print('No cube')
				return False

	print('Level completed')
	return True


def LoadDescription(world, toLoad):
	path = os.path.join('levels', toLoad)
	print(path)
	with open(path) as data_file:
		level = json.load(data_file)

	world.main.menuManager.destroyAll()
	menu = Menu(level['name'])
	dim = world.main.dimensions
	rect = (dim[0] / 2 - 400,dim[1] / 2 - 250,800,500)
	menu.setRect(rect)
	menu.setCaption('Level Description')
	for line in level['goalDescription']:
		menu.addItem(name=line, Type='label')

	world.main.menuManager.addMenu(menu)



class World():
	def __init__(self,main, level):
		self.main = main
		self.dimensions = main.dimensions
		self.code = Code(self)
		self.level = level
		self.char = Char(self)
		self.code.char = self.char

		self.screenRect = (0, 0, self.main.dimensions[0], self.main.dimensions[1])

		self.buttons = []

		pos = (10,10)
		b = Button(self,pos)
		b.action = self.loadDesc
		b.setCaption('To Do')
		self.buttons.append(b)

		pos = (100,10)
		b = Button(self,pos)
		b.action = self.loadCubes
		b.setCaption('Reset')
		self.buttons.append(b)

		pos = (200,10)
		b = Button(self,pos)
		b.action = self.main.loadLevelSelect
		b.setCaption('Level Select')
		self.buttons.append(b)




		self.loadDesc()
		self.loadCubes()

	def draw(self):
		self.code.draw()
		self.char.draw()

		surface = self.main.screen

		for btn in self.buttons:
			btn.draw(surface, mainRect=self.screenRect)

	def loadDesc(self):
		LoadDescription(self, self.level)

	def loadCubes(self):
		LoadLevel(self, self.level)
		self.char.reset()

	def checkCompletion(self):
		if checkCompletion(self,self.level):
			self.main.menuManager.destroyAll()
			menu = Menu('levelCompleted')
			dim = self.main.dimensions
			rect = (dim[0] / 2 - 200,dim[1] / 2 - 150,400,300)
			menu.setRect(rect)
			menu.setCaption('Level Completed!')
			menu.addItem(name='You have succesfully completed this level!', Type='label')
			menu.addItem(name='Level Select', Type='button', target=self.main.loadLevelSelect)
			self.main.menuManager.addMenu(menu)


	def mouseDown(self,pos):
		self.code.mouseDown(pos)

	def mouseUp(self,pos):
		self.code.mouseUp(pos)
		for btn in self.buttons:
			btn.checkPress(pos, mainRect=self.screenRect)

	def mouseUpdate(self, pos):
		self.code.mouseUpdate(pos)

	def rightClick(self,pos):
		self.code.rigthClick(pos)

	def update(self):
		pass
