import pygame
import time
from threading import Thread
from menu import Menu
from button import Button

colorDict = {'Red' : (255,0,0),
			 'Green' : (0,255,0),
			 'Blue' : (0,0,255)}

sleepInterval = 0.2

scrollSpeed = 20

def range_overlap(a_min, a_max, b_min, b_max):
			overlapping = True
			if (a_min > b_max) or (a_max < b_min):
				overlapping = False
			return overlapping

class codeBlock():
	def __init__(self):
		rect = self.code.rect
		self.offset = (rect[0] + 10, rect[1]+70)
		self.main = False
		if self.real:
			self.code.allBlocks.append(self)
		else:
			self.parent = None

	def checkRightClick(self, pos):
		rect = self.rect
		if range_overlap(rect[0],rect[2] + rect[0],pos[0],pos[0]) and range_overlap(rect[1],rect[3] + rect[1],pos[1],pos[1]):
			self.settingsMenu()

	def becomeReal(self):
		self.real = True
		self.code.allBlocks.append(self)

	def appendHover(self, pos, block):
		surface = self.code.world.main.screen
		rect = (self.rect[0],self.rect[1] - 5,self.rect[2],5)

		if range_overlap(rect[0],rect[2] + rect[0],pos[0],pos[0]) and range_overlap(rect[1],rect[3] + rect[1],pos[1],pos[1]):
			hRect = (rect[0] + (rect[2] /2) - 10, rect[1] - 4, 20,5)
			pygame.draw.rect(surface,(0,0,255), rect ,0)
			if not self.main:
				self.code.appendInfo = (self.parent, self, 'above')

		elif type(self) == loopBlock:
			rect = (self.rect[0],self.rect[1] + self.YLength - 10,self.rect[2],5)
			if range_overlap(rect[0],rect[2] + rect[0],pos[0],pos[0]) and range_overlap(rect[1],rect[3] + rect[1],pos[1],pos[1]):
				pygame.draw.rect(surface,(0,0,255), rect ,0)
				if not self.main:
					self.code.appendInfo = (self.parent, self, 'below')

			rect = (self.rect[0],self.rect[1] + self.rect[3],self.rect[2],5)
			if range_overlap(rect[0],rect[2] + rect[0],pos[0],pos[0]) and range_overlap(rect[1],rect[3] + rect[1],pos[1],pos[1]):
				pygame.draw.rect(surface,(0,0,255), rect ,0)
				self.code.appendInfo = (self, self, 'inside')
		elif type(self) == ifBlock:
			rect = (self.rect[0],self.rect[1] + self.YLength - 10,self.rect[2],5)
			if range_overlap(rect[0],rect[2] + rect[0],pos[0],pos[0]) and range_overlap(rect[1],rect[3] + rect[1],pos[1],pos[1]):
				pygame.draw.rect(surface,(0,0,255), rect ,0)
				if not self.main:
					self.code.appendInfo = (self.parent, self, 'below')

			rect = (self.rect[0],self.rect[1] + self.rect[3],self.rect[2],5)
			if range_overlap(rect[0],rect[2] + rect[0],pos[0],pos[0]) and range_overlap(rect[1],rect[3] + rect[1],pos[1],pos[1]):
				pygame.draw.rect(surface,(0,0,255), rect ,0)
				self.code.appendInfo = (self, self, 'insideTrue')

			rect = (self.rect[0],self.rect[1] + self.yFalseBegin- 10,self.rect[2],5)
			if range_overlap(rect[0],rect[2] + rect[0],pos[0],pos[0]) and range_overlap(rect[1],rect[3] + rect[1],pos[1],pos[1]):
				pygame.draw.rect(surface,(0,0,255), rect ,0)
				self.code.appendInfo = (self, self, 'insideFalse')

		else:
			rect = (self.rect[0],self.rect[1] + self.rect[3],self.rect[2],5)
			if range_overlap(rect[0],rect[2] + rect[0],pos[0],pos[0]) and range_overlap(rect[1],rect[3] + rect[1],pos[1],pos[1]):
				hRect = (rect[0] + (rect[2] /2) - 10, rect[1] - 4, 20,5)
				pygame.draw.rect(surface,(0,0,255), rect ,0)
				self.code.appendInfo = (self.parent, self, 'below')


		return False

	def destroy(self):
		self.code.allBlocks.pop(self)

	def getParentIndex(self):
		if type(self.parent) != ifBlock:
			counter = -1
			print(self.parent)
			for i in self.parent.codeBlocks:
				counter += 1
				if i == self:
					return (counter, None)
		else:
			counter = -1
			for i in self.parent.codeTrueBlocks:
				counter += 1
				if i == self:
					return (counter, True)

			counter = -1
			for i in self.parent.codeFalseBlocks:
				counter += 1
				if i == self:
					return (counter, False)

		print('Logic error, couldn\'t find self in parent')

	def setParent(self,parent):
		self.parent = parent

	def addChild(self,child, posIndex, extraInfo=None):
		if type(self) != ifBlock:
			if child.parent != None:
				child.parent.codeBlocks.pop(child.getParentIndex())
			else:
				self.code.spawnDummyBlocks()

			child.setParent(self)
			if not child.real:
				child.becomeReal()

			self.codeBlocks.insert(posIndex, child)
		else:
				#(parent, self, 'below')
			print('Attempting if add..')
			print(extraInfo)
			if child.parent != None:
				c = child.getParentIndex()
				if c[1]:
					child.parent.codeTrueBlocks.pop(c[0])
				else:
					child.parent.codeFalseBlocks.pop(c[0])
			else:
				self.code.spawnDummyBlocks()
			if not child.real:
				child.becomeReal()
			child.setParent(self)

			try:
				print(extraInfo[1].getParentIndex()[1])
			except:
				pass
			if (extraInfo[2] == 'insideTrue'):
				self.codeTrueBlocks.insert(posIndex, child)
			elif (extraInfo[2] == 'insideFalse'):
				self.codeFalseBlocks.insert(posIndex, child)
			elif extraInfo[1].getParentIndex()[1]:
				self.codeTrueBlocks.insert(posIndex, child)
			else:
				self.codeFalseBlocks.insert(posIndex, child)


	def setPos(self, pos):
		self.rect = (pos[0],pos[1], self.rect[2],self.rect[3])

	#Gets ovewritten when block is of type loopBlock
	def checkPickUp(self, pos):
		rect = self.rect
		if range_overlap(rect[0],rect[2] + rect[0],pos[0],pos[0]) and range_overlap(rect[1],rect[3] + rect[1],pos[1],pos[1]):
			self.code.spawnItem = self
			self.code.makeDummyBlock(self, pos)
			print('Picked up ',self)

class actionBlock(codeBlock):
	def __init__(self, code, real=True, action='Grab'):
		self.code = code
		self.real = real
		codeBlock.__init__(self)

		self.action = action

		self.caption = self.code.nameFont.render(self.action, True, (0,0,0))

		self.color = (153, 255, 102)

	def settingsMenu(self):
		menu = Menu('edit')
		dim = self.code.world.main.dimensions
		rect = (dim[0] / 2 - 200,dim[1] / 2 - 150,400,300)
		menu.setRect(rect)
		menu.setCaption('Edit Action Properties')
		menu.addItem(name='Action type:', Type='selection',value=self.action,selectionList=['Grab','Drop'], target=self)
		self.code.world.main.menuManager.addMenu(menu)

	def setValue(self, value):
		self.action = value
		self.caption = self.code.nameFont.render(self.action, True, (0,0,0))

	def draw(self, rect=None):
		if rect == None:
			rect = self.rect

		surface = self.code.world.main.screen
		rect = (rect[0],rect[1], self.caption.get_width(),rect[3])
		pygame.draw.rect(surface,self.color, rect,0)
		pos = (rect[0],rect[1])
		surface.blit(self.caption,(pos[0],pos[1] - 1))

		self.rect = rect
		return 30

	def exec(self):
		self.code.char.performAction(self.action)


class loopBlock(codeBlock):
	def __init__(self, code, main=False, real=True):
		self.code = code
		self.real = real
		codeBlock.__init__(self)
		self.main = main

		self.codeBlocks = []

		self.setLoopCount(5)

		self.color = (249,255,74)

	def checkPickUp(self, pos):
		if not self.main:
			rect = self.rect
			if range_overlap(rect[0],rect[2] + rect[0],pos[0],pos[0]) and range_overlap(rect[1],rect[3] + rect[1],pos[1],pos[1]):
				self.code.spawnItem = self
				self.code.makeDummyBlock(self, pos)
				print('Picked up ',self)

	def settingsMenu(self):
		menu = Menu('edit')
		dim = self.code.world.main.dimensions
		rect = (dim[0] / 2 - 200,dim[1] / 2 - 150,400,300)
		menu.setRect(rect)
		menu.setCaption('Edit Loop Properties')
		d = []
		for i in range(1,10):
			d.append(str(i))
		menu.addItem(name='Loop count:', Type='selection',value=str(self.loopCount),selectionList=d,target=self)
		self.code.world.main.menuManager.addMenu(menu)
	def setValue(self,value):
		self.setLoopCount(int(value))

	def setLoopCount(self,count):
		self.loopCount = count
		if self.main:
			self.caption = self.code.nameFont.render('Main Loop ' + str(count), True, (0,0,0))
		else:
			self.caption = self.code.nameFont.render('Loop '  + str(count), True, (0,0,0))


	def exec(self):
		if self.main:
			mainOldColor = self.color
			self.color = (0,255,0)
		for loop in range(0,self.loopCount):
			for i in self.codeBlocks:
				oldColor = i.color
				i.color = (255, 119, 243)
				i.exec()
				if self.code.breakThread:
					i.color = oldColor
					if self.main:
						self.color = mainOldColor
					return
				time.sleep(sleepInterval)
				i.color = oldColor
				if self.main:
					self.color = mainOldColor
		self.code.world.checkCompletion()



	def draw(self, rect=None):
		currentY = 30;
		if self.main:
			rect = (self.offset[0], self.offset[1],self.caption.get_width(),20)
			self.rect = rect
		else:
			self.offset = rect
			self.rect = rect

		if rect == None:
			rect = self.rect



		surface = self.code.world.main.screen
		pygame.draw.rect(surface,self.color, rect,0)
		pos = (rect[0],rect[1])

		for block in self.codeBlocks:
			rect = (self.offset[0] + 30, self.offset[1] + currentY,self.caption.get_width(),20)
			currentY += block.draw(rect)


		rect = (self.offset[0], self.offset[1] + currentY,self.caption.get_width(),20)
		pygame.draw.rect(surface,self.color, rect,0)
		rect = (self.offset[0], self.offset[1],20,currentY)
		pygame.draw.rect(surface,self.color, rect,0)
		surface.blit(self.caption,(pos[0],pos[1] - 1))
		currentY += 30
		self.YLength = currentY
		return currentY

class ifBlock(codeBlock):
	def __init__(self, code, real=True):
		self.code = code
		self.real = real
		codeBlock.__init__(self)
		self.caption = self.code.nameFont.render('If carrying', True, (0,0,0))
		self.setValue('Red')
		self.false = self.code.nameFont.render('Else:', True, (0,0,0))
		self.color = (249,255,74)

		self.codeTrueBlocks = []
		self.codeFalseBlocks = []

	def settingsMenu(self):
		menu = Menu('edit')
		dim = self.code.world.main.dimensions
		rect = (dim[0] / 2 - 200,dim[1] / 2 - 150,400,300)
		menu.setRect(rect)
		menu.setCaption('Edit If Properties')
		menu.addItem(name='Carrying', Type='selection',value='Red',selectionList=['Red','Green','Blue'], target=self)
		self.code.world.main.menuManager.addMenu(menu)

	def setValue(self, value):
		self.condition = value
		self.conditionText = self.code.nameFont.render(value + ':', True, colorDict[value])

	def draw(self, rect=None):
		currentY = 30;
		self.offset = rect
		self.rect = rect

		if rect == None:
			rect = self.rect



		surface = self.code.world.main.screen
		rect = (self.offset[0], self.offset[1],self.caption.get_width() + self.conditionText.get_width(),20)
		pygame.draw.rect(surface,self.color, rect,0)
		pos = (rect[0],rect[1])

		for block in self.codeTrueBlocks:
			rect = (self.offset[0] + 30, self.offset[1] + currentY,self.caption.get_width() + self.conditionText.get_width(),20)
			currentY += block.draw(rect)


		rect = (self.offset[0], self.offset[1] + currentY,self.caption.get_width() + self.conditionText.get_width(),20)

		falseRect = rect
		pygame.draw.rect(surface,self.color, rect,0)
		currentY += 30
		self.yFalseBegin = currentY


		for block in self.codeFalseBlocks:
			rect = (self.offset[0] + 30, self.offset[1] + currentY,self.caption.get_width() + self.conditionText.get_width(),20)
			currentY += block.draw(rect)

		rect = (self.offset[0], self.offset[1] + currentY,self.caption.get_width() + self.conditionText.get_width(),20)
		pygame.draw.rect(surface,self.color, rect,0)

		rect = (self.offset[0], self.offset[1],20,currentY)
		pygame.draw.rect(surface,self.color, rect,0)
		surface.blit(self.caption,(pos[0],pos[1] - 1))
		surface.blit(self.conditionText,(pos[0] + self.caption.get_width(),pos[1] - 1))
		surface.blit(self.false,(falseRect[0],falseRect[1] - 1))
		currentY += 30
		self.YLength = currentY
		return currentY

	def exec(self):
		if self.code.char.getCarryColor() == colorDict[self.condition]:
			for i in self.codeTrueBlocks:
					oldColor = i.color
					i.color = (255, 119, 243)
					i.exec()
					if self.code.breakThread:
						i.color = oldColor
						return
					time.sleep(sleepInterval)
					i.color = oldColor
			self.code.world.checkCompletion()
		else:
			for i in self.codeFalseBlocks:
					oldColor = i.color
					i.color = (255, 119, 243)
					i.exec()
					if self.code.breakThread:
						i.color = oldColor
						return
					time.sleep(sleepInterval)
					i.color = oldColor
			self.code.world.checkCompletion()




class printBlock(codeBlock):
	def __init__(self, code, real=True):
		self.code = code
		self.real = real

		codeBlock.__init__(self)

		self.caption = self.code.nameFont.render('Print', True, (0,0,0))
		self.print = self.code.nameFont.render(str(self), True, (0,255,0))
		self.toPrint = str(self)

		self.color = (255,0,0)

	def settingsMenu(self):
		menu = Menu('edit')
		dim = self.code.world.main.dimensions
		rect = (dim[0] / 2 - 200,dim[1] / 2 - 150,400,300)
		menu.setRect(rect)
		menu.setCaption('Edit Print Properties')
		self.code.world.main.menuManager.addMenu(menu)

	def setPrint(self, prnt):
		self.toPrint = prnt
		self.print = self.code.nameFont.render(prnt, True, (0,255,0))

	def draw(self, rect=None):
		if rect == None:
			rect = self.rect

		surface = self.code.world.main.screen
		rect = (rect[0],rect[1], self.caption.get_width() + self.print.get_width(),rect[3])
		pygame.draw.rect(surface, self.color, rect,0)
		pos = (rect[0],rect[1])
		surface.blit(self.caption,(pos[0],pos[1] - 1))
		surface.blit(self.print,(pos[0] + self.caption.get_width(),pos[1] - 1))

		self.rect = rect
		return 30

	def exec(self):
		print(self.toPrint)



class moveBlock(codeBlock):
	def __init__(self, code, real=True):
		self.code = code
		self.real = real
		codeBlock.__init__(self)

		self.caption = self.code.nameFont.render('Move', True, (0,0,0))
		self.move = self.code.nameFont.render('Up', True, (0,255,0))
		self.toMove = str('Up')

		self.color = (0, 153, 255)

	def settingsMenu(self):
		menu = Menu('edit')
		dim = self.code.world.main.dimensions
		rect = (dim[0] / 2 - 200,dim[1] / 2 - 150,400,300)
		menu.setRect(rect)
		menu.setCaption('Edit Move Properties')
		menu.addItem(name='Move: ', Type='selection',value=self.toMove,selectionList=['Up','Down'], target=self)
		self.code.world.main.menuManager.addMenu(menu)

	def setMove(self, move):
		self.toMove = move
		self.move = self.code.nameFont.render(move, True, (0,255,0))

	def setValue(self, value):
		self.setMove(value)

	def draw(self, rect=None):
		if rect == None:
			rect = self.rect

		surface = self.code.world.main.screen
		rect = (rect[0],rect[1], self.caption.get_width() + self.move.get_width(),rect[3])
		pygame.draw.rect(surface,self.color, rect,0)
		pos = (rect[0],rect[1])
		surface.blit(self.caption,(pos[0],pos[1] - 1))
		surface.blit(self.move,(pos[0] + self.caption.get_width(),pos[1] - 1))

		self.rect = rect
		return 30

	def exec(self):
		self.code.char.move(self.toMove)




class Code():
	def __init__(self, world):
		self.world = world
		self.rect = (self.world.dimensions[0] / 2, 0,self.world.dimensions[0] / 2, self.world.dimensions[1])

		rect = self.rect
		self.devideLinePos = [(rect[0] + (rect[2] / 2), rect[1]), (rect[0] + (rect[2] / 2), rect[3])]

		self.titleFont = pygame.font.SysFont("verdana", 24)
		self.nameFont = pygame.font.SysFont("verdana", 18)
		self.caption = self.titleFont.render("Code", True, (0, 0, 0))

		self.allBlocks = []

		self.mainBlocks = []
		self.mainBlock = loopBlock(self, main=True)
		self.mainBlock.setLoopCount(1)
		self.mainBlocks.append(self.mainBlock)

		self.breakThread = False

		self.dummyBlocks = []
		self.dummyBlocks.append(actionBlock(self,real=False))
		self.dummyBlocks.append(moveBlock(self,real=False))
		self.dummyBlocks.append(loopBlock(self,real=False))
		self.dummyBlocks.append(ifBlock(self,real=False))
		self.spawnDummyBlocks()

		self.char = None

		self.buttons = []
		pos = (self.rect[0] + (self.rect[2] / 4),self.rect[1] + 70)
		b = Button(self,pos)
		b.action = self.runCode
		b.setCaption('Run')
		self.buttons.append(b)

		self.spawnItem = None

		self.codeThread = Thread(target=self.mainBlock.exec)

	def scrollUp(self):
		offset = self.mainBlocks[0].offset
		self.mainBlocks[0].offset = (offset[0],offset[1] + scrollSpeed)
		if self.mainBlocks[0].offset[1] < (-1 * self.mainBlocks[0].YLength + 70):
			self.mainBlocks[0].offset = (offset[0],-1 * self.mainBlocks[0].YLength + 70)

	def scrollDown(self):
		offset = self.mainBlocks[0].offset
		self.mainBlocks[0].offset = (offset[0],offset[1] - scrollSpeed)
		if self.mainBlocks[0].offset[1] > 200:
			self.mainBlocks[0].offset = (offset[0],200)

	def spawnDummyBlocks(self):
		offset = (self.rect[0] + (self.rect[2] / 2), self.rect[1] + 70)


		self.dummyBlocks[0] = actionBlock(self,real=False)
		self.dummyBlocks[1] = moveBlock(self,real=False)
		self.dummyBlocks[2] = loopBlock(self,real=False)
		self.dummyBlocks[3] = ifBlock(self,real=False)



		offset = (self.rect[0] + (self.rect[2] / 2), self.rect[1] + 70)
		currentY = 30
		for block in self.dummyBlocks:
			rect = (offset[0] + 30, offset[1] + currentY,block.caption.get_width(),20)
			block.rect = rect
			currentY += 60


	def test(self):
		self.mainBlock.setLoopCount(2)
		p = printBlock(self)
		p.setPrint('Begin count')
		p.setParent(self.mainBlock)
		self.mainBlock.codeBlocks.append(p)

		d = loopBlock(self)
		d.setParent(self.mainBlock)
		for i in range(0,4):
				p = printBlock(self)
				p.setPrint('Hello from' + str(i) + '\'th')
				p.setParent(d)
				d.codeBlocks.append(p)
		self.mainBlock.codeBlocks.append(d)



	def draw(self):
		surface = self.world.main.screen
		rect = self.rect

		##BG
		pygame.draw.rect(surface,(109,209,186), rect,0)

		##DevideLine
		start_pos = self.devideLinePos[0]
		end_pos   = self.devideLinePos[1]
		pygame.draw.line(surface, (0,0,0), start_pos, end_pos, 2)

		##Text
		pos = (rect[0] + (rect[2] / 4) - self.caption.get_width(), rect[1])
		surface.blit(self.caption,pos)

		for block in self.mainBlocks:
			block.draw()

		for btn in self.buttons:
			btn.draw(surface)

		##Dummy Blocks
		for block in self.dummyBlocks:
			block.draw(block.rect)

	def mouseUpdate(self,pos):
		if self.spawnItem != None:
			self.spawnItem.setPos((-100,-100))

		self.appendInfo = None
		for block in self.allBlocks:
			if self.spawnItem != None:
				block.appendHover(pos, None)

		if self.spawnItem != None:
			self.spawnItem.setPos(pos)


	def mouseDown(self, pos):
		for btn in self.buttons:
			btn.checkPress(pos)

		for block in self.allBlocks:
			block.checkPickUp(pos)
		for block in self.dummyBlocks:
			block.checkPickUp(pos)


	def makeDummyBlock(self, item, pos):
		if item in self.dummyBlocks:
			return
		item.setPos(pos)
		self.dummyBlocks.append(item)
		if item.parent != None:
			if type(item.parent) != ifBlock:
				item.parent.codeBlocks.pop(item.getParentIndex()[0])
				item.parent = None
			else:
				if (item.getParentIndex()[1]):
					item.parent.codeTrueBlocks.pop(item.getParentIndex()[0])
					item.parent = None
				else:
					item.parent.codeFalseBlocks.pop(item.getParentIndex()[0])
					item.parent = None

	def mouseUp(self,pos):
		if self.spawnItem != None:
			if self.appendInfo != None:
				print(self.appendInfo)
					#self.code.appendInfo = (self.parent, self, 'below')
				if True:

					if   self.appendInfo[2] == 'inside':
						self.appendInfo[0].addChild(self.spawnItem, 0, extraInfo=self.appendInfo)
					if   (self.appendInfo[2] == 'insideTrue') or (self.appendInfo[2] == 'insideFalse'):
						self.appendInfo[0].addChild(self.spawnItem, 0, extraInfo=self.appendInfo)
					elif self.appendInfo[2] == 'below':
						self.appendInfo[0].addChild(self.spawnItem, self.appendInfo[1].getParentIndex()[0] + 1, extraInfo=self.appendInfo)
					elif self.appendInfo[2] == 'above':
						self.appendInfo[0].addChild(self.spawnItem, self.appendInfo[1].getParentIndex()[0], extraInfo=self.appendInfo)
					print('Dropped')

					if self.spawnItem in self.dummyBlocks:
						self.dummyBlocks.remove(self.spawnItem)
						print('Removed drom dummyBlocks')

				else:
					print('Cant drop')

			else:
				self.makeDummyBlock(self.spawnItem, pos)


		self.spawnItem = None

	def rigthClick(self,pos):
		for block in self.allBlocks:
			block.checkRightClick(pos)



	def runCode(self):


		if self.codeThread.isAlive():
			self.breakThread = True
			while self.codeThread.isAlive():
				pass
			self.breakThread = False

		self.world.loadCubes()
		self.codeThread = Thread(target=self.mainBlock.exec)
		self.codeThread.start()
