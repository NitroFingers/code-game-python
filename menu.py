import pygame

def range_overlap(a_min, a_max, b_min, b_max):
			overlapping = True
			if (a_min > b_max) or (a_max < b_min):
				overlapping = False
			return overlapping

class Button:
	def __init__(self,parent, pos):
		self.parent = parent
		
		self.pos = pos
		self.action = None
		self.args = None


	def setCaption(self, caption):
		font = pygame.font.SysFont("verdana", 18)
		self.caption = font.render(caption, True, (0,0,0))

	def draw(self, surface, mainRect):
		pos = (self.pos[0] + mainRect[0],self.pos[1] + mainRect[1])
		rect = (pos[0], pos[1],self.caption.get_width(),20)
		pygame.draw.rect(surface, (250,37,40), rect,0)
		surface.blit(self.caption,pos)

	def checkPress(self, pos, mainRect):
		pos = (pos[0] - mainRect[0], pos[1] - mainRect[1])
		rect = (self.pos[0],self.pos[1],self.caption.get_width(),20)
		if range_overlap(rect[0],rect[2] + rect[0],pos[0],pos[0]) and range_overlap(rect[1],rect[3] + rect[1],pos[1],pos[1]): 
			if self.action is not None:
				if self.args  is None:
					self.action()
				else:
					self.action(self.args)
			else:
				print(self, ' has no action')
			return True
		return False

class MenuItem:
	def __init__(self):
		pass

class ItemLabel(MenuItem):
	def __init__(self, menu, caption='None'):
		MenuItem.__init__(self)
		self.pos = (0,0)
		self.font = pygame.font.SysFont("verdana", 18)
		self.menu = menu
		self.caption = caption

	def draw(self, surface, mainRect):
		pos = (self.pos[0] + mainRect[0], self.pos[1] + mainRect[1])
		caption =   self.font.render(self.caption  , True, (0,0,0))
		surface.blit(caption, pos)

class ItemDynLabel(MenuItem):
	def __init__(self, menu, target=None):
		MenuItem.__init__(self)
		self.pos = (0,0)
		self.font = pygame.font.SysFont("verdana", 18)
		self.menu = menu
		self.target = target

	def draw(self, surface, mainRect):
		pos = (self.pos[0] + mainRect[0], self.pos[1] + mainRect[1])
		caption =   self.font.render(self.target()  , True, (0,0,0))
		surface.blit(caption, pos)

class ItemSelection(MenuItem):
	def __init__(self, menu):
		MenuItem.__init__(self)
		self.pos = (0,0)
		self.font = pygame.font.SysFont("verdana", 18)
		self.menu = menu
		self.caption = 'None'
		self.value = 'None'
		self.valueList = ['1','2']
		self.curSelection = 0

		self.cycleButton = Button(self,(0,0))
		self.cycleButton.action = self.cycle
		self.cycleButton.setCaption('Cycle')

		self.menu.buttons.append(self.cycleButton)

		self.targetEdit = None

	def draw(self, surface, mainRect):

		caption = self.font.render(self.caption, True, (0,0,0))
		value =   self.font.render(self.value  , True, (0,255,0))

		pos = (self.pos[0] + mainRect[0], self.pos[1] + mainRect[1])
		surface.blit(caption, pos)
		pos = (pos[0] + caption.get_width() + 3, pos[1])
		surface.blit(value, pos)
		pos = (pos[0] + value.get_width() + 10, pos[1])
		self.cycleButton.pos = (pos[0] - mainRect[0], pos[1] - mainRect[1])

	def cycle(self):
		self.curSelection += 1
		if self.curSelection == len(self.valueList):
			self.curSelection = 0
		self.value = self.valueList[self.curSelection]
		self.targetEdit.setValue(self.value)



class Menu():
	def __init__(self, Type):
		self.type = Type
		self.rect = (0,0,100,100)
		self.color = (206, 225, 255)

		self.font = pygame.font.SysFont("verdana", 18)
		self.setCaption('None...')

		self.buttons = []

		self.settings = []
		self.returnFunct = None

		self.yOffset = 25

		self.grabbed = False
		self.grabOffset = (0,0)

	def update(self, mousePos, mm):
		if self.grabbed:
			pygame.draw.rect(mm.surface, (255,255,255), self.rect,0)
			mm.main.dirtyRects.append(self.rect)
			self.rect = (mousePos[0] + self.grabOffset[0],mousePos[1] + self.grabOffset[1], self.rect[2],self.rect[3])

	def cannotClose(self):
		self.buttons.pop(0)

	def addItem(self, name='None', Type=None,value='None',selectionList=[], target=None, args=None):
		if Type == 'selection':
			rect = self.rect

			item = ItemSelection(self)
			item.caption = name
			item.value = value
			item.valueList = selectionList
			item.pos = (5, self.yOffset)
			item.targetEdit = target
			self.settings.append(item)
			self.yOffset += 25

		if Type == 'button':
			rect = self.rect
			btn = Button(self,(5, self.yOffset))
			btn.setCaption(name)
			btn.action = target
			btn.args = args
			self.buttons.append(btn)
			self.yOffset += 25

		if Type == 'label':
			rect = self.rect
			item = ItemLabel(self,caption=name)
			item.pos = (5, self.yOffset)
			self.settings.append(item)
			self.yOffset += 25

		if Type == 'dyn_label':
			rect = self.rect
			item = ItemDynLabel(self,target=target)
			item.pos = (5, self.yOffset)
			self.settings.append(item)
			self.yOffset += 25


	def draw(self,surface):
		pygame.draw.rect(surface,self.color, self.rect ,0)

		pos = (self.rect[0],self.rect[1])
		rect = (self.rect[0],self.rect[1],self.rect[2],22)
		pygame.draw.rect(surface,(159, 177, 206), rect ,0)
		surface.blit(self.title,pos)

		for btn in self.buttons:
			btn.draw(surface, self.rect)

		for item in self.settings:
			item.draw(surface, self.rect)

	def setRect(self,rect):
		self.rect = rect

		
		btn = Button(self,(0,0))
		btn.setCaption('Close')
		pos = (rect[2] - btn.caption.get_width(), 0)
		btn.pos = pos
		btn.action = self.close
		self.buttons.append(btn)

	def close(self):
		self.menuManager.menus.remove(self)

	def setCaption(self,caption):
		self.title = self.font.render(caption, True, (0, 0, 0))

	def ClickEvent(self,pos):

		for btn in self.buttons:
			if btn.checkPress(pos, self.rect):
				self.bringToFront()
				return True
		rect = self.rect

		if range_overlap(rect[0],rect[2] + rect[0],pos[0],pos[0]) and range_overlap(rect[1], rect[1] + 22, pos[1], pos[1]):
			self.grabbed = True
			self.grabOffset = (rect[0] - pos[0], rect[1] - pos[1])
			self.bringToFront()


		if range_overlap(rect[0],rect[2] + rect[0],pos[0],pos[0]) and range_overlap(rect[1],rect[3] + rect[1],pos[1],pos[1]):
			self.bringToFront()
			return True
		return False

	def UpEvent(self):
		self.grabbed = False

	def bringToFront(self):
		a = self.menuManager.menus
		if self in a:
			a.remove(self)
			a.append(self)


class MenuManager():
	def __init__(self, main):
		self.main = main

		self.menus = []

		self.surface = self.main.screen

	def draw(self):
		for menu in self.menus:
			menu.draw(self.surface)

	def update(self, mousePos):
		for menu in self.menus:
			menu.update(mousePos, self)

	def addMenu(self,newMenu):
		newMenu.menuManager = self
		self.menus.append(newMenu)

	def destroyAll(self):
		self.menus = []

	def mouseDown(self,pos):
		for menu in reversed(self.menus):
			if menu.ClickEvent(pos):
				return True
		return False

	def mouseUp(self):
		for menu in self.menus:
			menu.UpEvent()
			

		